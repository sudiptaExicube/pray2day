import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'intro',
    pathMatch: 'full'
  },
  {
    path: 'home-page',
    loadChildren: () => import('./screens/home-page/home-page.module').then( m => m.HomePagePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./screens/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'registration',
    loadChildren: () => import('./screens/registration/registration.module').then( m => m.RegistrationPageModule)
  },
  {
    path: 'editprofile',
    loadChildren: () => import('./screens/editprofile/editprofile.module').then( m => m.EditprofilePageModule)
  },
  {
    path: 'transaction-history',
    loadChildren: () => import('./screens/transaction-history/transaction-history.module').then( m => m.TransactionHistoryPageModule)
  },
  {
    path: 'otp-verification',
    loadChildren: () => import('./screens/otp-verification/otp-verification.module').then( m => m.OtpVerificationPageModule)
  },
  {
    path: 'success',
    loadChildren: () => import('./screens/success/success.module').then( m => m.SuccessPageModule)
  },
  {
    path: 'intro',
    loadChildren: () => import('./screens/intro/intro.module').then( m => m.IntroPageModule)
  },
  {
    path: 'subscription',
    loadChildren: () => import('./screens/subscription/subscription.module').then( m => m.SubscriptionPageModule)
  },
  {
    path: 'my-subscription',
    loadChildren: () => import('./screens/my-subscription/my-subscription.module').then( m => m.MySubscriptionPageModule)
  },
  {
    path: 'change-password',
    loadChildren: () => import('./screens/change-password/change-password.module').then( m => m.ChangePasswordPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./screens/forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
  {
    path: 'prayer-subpage',
    loadChildren: () => import('./screens/prayer-subpage/prayer-subpage.module').then( m => m.PrayerSubpagePageModule)
  },
  {
    path: 'prayer-view',
    loadChildren: () => import('./screens/prayer-view/prayer-view.module').then( m => m.PrayerViewPageModule)
  },
  {
    path: 'reset-password',
    loadChildren: () => import('./screens/reset-password/reset-password.module').then( m => m.ResetPasswordPageModule)
  },
  {
    path: 'payment-screen',
    loadChildren: () => import('./screens/payment-screen/payment-screen.module').then( m => m.PaymentScreenPageModule)
  },
  {
    path: 'privicy',
    loadChildren: () => import('./screens/privicy/privicy.module').then( m => m.PrivicyPageModule)
  },
  {
    path: 'terms',
    loadChildren: () => import('./screens/terms/terms.module').then( m => m.TermsPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./screens/about/about.module').then( m => m.AboutPageModule)
  },
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
