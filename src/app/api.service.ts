import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalService } from './global.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  public baseUrl: string = "https://ph8.projectdemoclient.xyz/pray2day/public/api/"

  constructor(
    public http: HttpClient,
    // private navCtrl: NavController,
    // public zone: NgZone,
    private global: GlobalService,
  ) { }

  apiWithToken(endPointName:any) {
    console.log('token', this.global.user_token)
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.global.user_token,
        "Accept":"application/json"
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + endPointName, {}, httpOptions).subscribe(data => {
        //console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        resolve(err);
      });
    });
  }

  apiWithTokenBody(endPointName:any,paramFields:any) {
    console.log('token', this.global.user_token);
    console.log('token', paramFields);
    console.log(`${this.baseUrl}` + endPointName)
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.global.user_token,
        "Accept":"application/json"
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + endPointName, paramFields, httpOptions)
      
      // .subscribe(data => {
      //  // console.log(data);
      //   resolve(data);  
      // }, err => {
      //   console.log(err);
      //   resolve(err);
      // });
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => { resolve(err); },    // errorHandler 
        next: (data) => { resolve(data); },  
      });


    });
  }


  /* == Global data fetch API == */
  public GlobalDataFunction(isloading: boolean){
      // if(isloading){ this.global.presentLoadingDefault() }

      this.http.get(`${this.baseUrl}` + 'auth/signup/master')
      .subscribe({
        complete: () => { 
          if(isloading){ this.global.presentLoadingClose() }  
          console.log("Complete .....");
        }, // completeHandler
        error: (err:any) => {
          if(isloading){ this.global.presentLoadingClose(); this.global.presentToast(JSON.stringify(err),null); }
          console.log("Fetch global data error", err);
        },    // errorHandler 
        next: (data:any) => { 
          console.log("Fetch global data success : ", data)
          if(isloading){ this.global.presentLoadingClose() }
          if(data){
            if(data.status === 'success'){
              this.global.app_globalData = data.data;
            }
          }
        },  
      });
  }

  /* == LOGIN API == */
  public signinFunction(params:any){
    // let fd = new FormData();
    // fd.append("email", params.email);
    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + 'auth/login', params)
      // .subscribe(data => {
      //   resolve(data);
      // }, err => {
      //   console.log(err);
      //   resolve(err);
      // });
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => { resolve(err); },    // errorHandler 
        next: (data) => { resolve(data); },  
     });

    });
  }

  /* == REGISTRATION API == */
  public registrationFunction(params:any){
    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + 'auth/signup', params)
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => { resolve(err); },    // errorHandler 
        next: (data) => { resolve(data); },  
      });

    });
  }

  /* == OTP verification API == */
  public otpVerificationFunction(params:any){
    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + 'otp/verify', params)
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => { resolve(err); },    // errorHandler 
        next: (data) => { resolve(data); },  
      });

    });
  }

  /* == RESEND OTP API == */
  public resendOtpFunction(params:any){
    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + 'otp/resend', params)
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => { resolve(err); },    // errorHandler 
        next: (data) => { resolve(data); },  
      });

    });
  }

  /* == FORGOT password API == */
  public forgotPasswordFunction(params:any){
    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + 'auth/password-reset', params)
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => { resolve(err); },    // errorHandler 
        next: (data) => { resolve(data); },  
      });

    });
  }

  /* == UPDATE profile details API == */
  public updateProfileFunction(params:any){
    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + 'user/profile/update', params)
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => { resolve(err); },    // errorHandler 
        next: (data) => { resolve(data); },  
      });

    });
  }


  public uploadProfileImage(endPointName:string, params:any){
    let fd = new FormData();
    fd.append("type", params.type);
    fd.append("profile_picture", params.imagePath);

        // fd.append('type', 'profile_image');
    // fd.append('file', blobdata, 'avatar.jpeg');
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.global.user_token,
        "Accept":"application/json",
        // 'Content-Type': 'multipart/form-data',
      })
    };

    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + endPointName, fd,httpOptions)
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => { resolve(err); },    // errorHandler 
        next: (data) => { resolve(data); },  
      });

    });


  }

  public getProfileInfo(){
    console.log('token', this.global.user_token)
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.global.user_token,
        "Accept":"application/json"
      })
    };
    
      this.http.post(`${this.baseUrl}` + 'user/profile/get', {}, httpOptions)
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => {
          console.log("User profile info fetch error : ", JSON.stringify(err))
        },    // errorHandler 
        next: (success:any) => { 
          if(success){
            let data = success.data;
            localStorage.setItem("user_details",JSON.stringify(data.user))
            this.global.userdetails = data.user;
          }
         },  
      });
  }

  public fetchHomeData(){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.global.user_token,
        "Accept":"application/json"
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + 'user/homepage/load', {}, httpOptions)
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => { resolve(err);},    // errorHandler 
        next: (success:any) => {resolve(success);},  
      });
    });
  }

  public fetchPrayerChapter(params:any){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.global.user_token,
        "Accept":"application/json"
      })
    };

    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + 'user/prayer/chapters', params, httpOptions)
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => { resolve(err);},    // errorHandler 
        next: (success:any) => {resolve(success);},  
      });
    });
  }

  /* == Ftech Chapter Quotes using chapter ID and line ID == */
  public fetch_chapterQuotes(params:any){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.global.user_token,
        "Accept":"application/json"
      })
    };

    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + 'user/prayer/line/details', params, httpOptions)
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => { resolve(err);},    // errorHandler 
        next: (success:any) => {resolve(success);},  
      });
    });
  }


  /* == Ftech subscription lists == */
  public api_fetchSubscriptions(){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.global.user_token,
        "Accept":"application/json"
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + 'user/subscription/fetch-plans', {}, httpOptions)
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => { resolve(err);},    // errorHandler 
        next: (success:any) => {resolve(success);},  
      });
    });
  }


  /* == Country lists ====== */
  public api_fetchCountries(){
    let httpOptions = {
      headers: new HttpHeaders({
        // 'Authorization': 'Bearer ' + this.global.user_token,
        "Accept":"application/json"
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + 'master/countries', {}, httpOptions)
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => { resolve(err);},    // errorHandler 
        next: (success:any) => {resolve(success);},  
      });
    });
  }

  /* == State lists ====== */
  public api_fetchStates(params:any){
    let httpOptions = {
      headers: new HttpHeaders({
        // 'Authorization': 'Bearer ' + this.global.user_token,
        "Accept":"application/json"
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + 'master/states', params, httpOptions)
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => { resolve(err);},    // errorHandler 
        next: (success:any) => {resolve(success);},  
      });
    });
  }  

  /* == Stripe payment ====== */
  public api_stripePayment(params:any){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.global.user_token,
        "Accept":"application/json"
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + 'user/subscription/purchase', params, httpOptions)
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => { resolve(err);},    // errorHandler 
        next: (success:any) => {resolve(success);},  
      });
    });
  }

  /* == Stripe payment ====== */
  public api_mySubscription(){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.global.user_token,
        "Accept":"application/json"
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + 'user/subscription/active-plan', {}, httpOptions)
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => { resolve(err);},    // errorHandler 
        next: (success:any) => {resolve(success);},  
      });
    });
  }


  /* == CMS content ============*/
  public api_fetchCMS(endPointName:any,params:any){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.global.user_token,
        "Accept":"application/json"
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + endPointName, params, httpOptions)
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => { resolve(err);},    // errorHandler 
        next: (success:any) => {resolve(success);},  
      });
    });
  }

  /* == Transaction history ============ */
  public api_transaction_history(){
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.global.user_token,
        "Accept":"application/json"
      })
    };
    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + 'user/subscription/history', {}, httpOptions)
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => { resolve(err);},    // errorHandler 
        next: (success:any) => {resolve(success);},  
      });
    });
  }


   /* == Transaction history ============ */
   public api_transaction_details(id:any){
    // let httpOptions = {
    //   headers: new HttpHeaders({
    //     'Authorization': 'Bearer ' + this.global.user_token,
    //     "Accept":"application/json"
    //   })
    // };
    // return new Promise(resolve => {
    //   this.http.post(`${this.baseUrl}` + 'user/subscription/history', {}, httpOptions)
    //   .subscribe({
    //     complete: () => { console.log("Complete .....") }, // completeHandler
    //     error: (err) => { resolve(err);},    // errorHandler 
    //     next: (success:any) => {resolve(success);},  
    //   });
    // });

    let fd = new FormData();
    fd.append("id", id);

        // fd.append('type', 'profile_image');
    // fd.append('file', blobdata, 'avatar.jpeg');
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.global.user_token,
        "Accept":"application/json",
        // 'Content-Type': 'multipart/form-data',
      })
    };

    return new Promise(resolve => {
      this.http.post(`${this.baseUrl}` + 'user/subscription/transaction/details', fd,httpOptions)
      .subscribe({
        complete: () => { console.log("Complete .....") }, // completeHandler
        error: (err) => { resolve(err); },    // errorHandler 
        next: (data) => { resolve(data); },  
      });

    });

  }
  

  


}
