import { Injectable, NgZone } from '@angular/core';
import {
	AlertController,
	LoadingController,
	MenuController,
	ModalController,
	NavController,
	Platform,
	ToastController
} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  public loading: any;
  public app_globalData:any = null;
  public userdetails:any;
  public user_token:any;
  public countryList:any=[];
  public subscriptionDetails:any=null;

  public passwordReset_type:string = ''

  public about_us:any = null;
  public terms:any = null;
  public privacy:any = null;
  public transaction_history:any = [];



  constructor(
		public loadingCtrl: LoadingController,
		private toastCtrl: ToastController,
		public alertController: AlertController,
		public platform: Platform,
    public menu: MenuController,
    public navCtrl: NavController,
    public zone:NgZone,
    public modalController:ModalController
  ) { }

  checkPlatformName() {
		return this.platform.is('ios') ? 'ios' : 'general'
	}

  openMenu(){
    this.menu.enable(true);
		this.menu.open();
  }

  closeMenu(){
    this.menu.enable(true);
		this.menu.close();
  }

  back(){
    this.navCtrl.pop()
  }

  //Validation
  isvalidEmailFormat(email: any) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  async showToast(txt: any) {
    const toast = await this.toastCtrl.create({
      message: txt,
      duration: 3000,
      position: 'bottom',
      cssClass: 'toast-custom'
    });
    toast.present();
  }

  async presentLoadingDefault() {
    // this.loading = await this.loadingCtrl.create({
    //   message: '',
    //   cssClass: 'loader-waiting'
    // });
    // await this.loading.present();

    this.loading = await this.loadingCtrl.create({ 
      spinner: null,
      message: `<img class="loading" width="30px" height="30px" src="../../assets/images/loading.gif" />`,
      translucent: true,
      cssClass: 'custom-loading-class',
      backdropDismiss: true
    });
     await this. loading.present();

  }

  presentLoadingClose() {
    this.loading.dismiss();
  }


  async presentToast(message: any,duration:any) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: duration ? duration : 3000,
      position: 'bottom'
    });
    toast.present();
  }

}
