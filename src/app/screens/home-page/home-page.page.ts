import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/global.service';
import { IonSlides } from '@ionic/angular';
import { NavigationExtras, Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.page.html',
  styleUrls: ['./home-page.page.scss'],
})
export class HomePagePage implements OnInit {
  public slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:true
   };
  public chapterArr: any = ["OUR FATHER CHILD CATHOLIC","SIGN OF THE CROSS CATHOLIC CHILD","LOREM IPSUM PRAYER 3" ,"LOREM IPSUM PRAYER 4"];
  
  public showLoading:boolean = false;
  public homeData:any;
  
  constructor(
    public global:GlobalService,
    private route: Router,
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.fetchHomePage_date('showskeleton')

  }

  public fetchHomePage_date(value:string) {
    if(value == 'showskeleton'){this.showLoading = true;}
    this.apiService.fetchHomeData()
    .then((response:any)=>{
      console.log("Home page response : ", response);
      if(value == 'showskeleton'){this.showLoading = false;}
      if(response.status == "success"){
        if(response.data){
          // let data = response.data;
          this.homeData = response.data
        }
      }else{
        this.homeData=[]
        this.global.presentToast(response.message,null);
      }
    })
    .catch(()=>{
      this.homeData=[]
      if(value == 'showskeleton'){this.showLoading = false;}
    })
  }

  selectChapter(item:any){
    console.log("selectChapter : ", item)
    const navigationExtras: NavigationExtras = {
      queryParams: {
        prayer_id: item.id
      }
    };
    this.route.navigate(['./prayer-subpage'],navigationExtras)
  }

  handleRefresh(event:any) {
    // setTimeout(() => {
    //   // Any calls to load data go here
    //   event.target.complete();
    // }, 2000);
    this.apiService.fetchHomeData()
    .then((response:any)=>{
      event.target.complete();
      if(response.status == "success"){
        if(response.data){
          // let data = response.data;
          this.homeData = response.data
        }
      }else{
        this.homeData=[]
        this.global.presentToast(response.message,null);
      }
    })
    .catch(()=>{
      this.homeData=[]
      event.target.complete();
    })

  }

}
