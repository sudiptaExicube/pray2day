import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/global.service';
import { Stripe } from '@awesome-cordova-plugins/stripe/ngx';
import { NavigationExtras, Router } from '@angular/router';
import { PaymentScreenPage } from '../payment-screen/payment-screen.page';
import { ApiService } from 'src/app/api.service';
import { InAppPurchase2 } from '@awesome-cordova-plugins/in-app-purchase-2/ngx';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.page.html',
  styleUrls: ['./subscription.page.scss'],
})
export class SubscriptionPage implements OnInit {

  public showLoading:boolean= false;
  public subscriptionList:any=[];
  public selectedIndex:any = 0;
  public selectedItemDetails:any={}

  constructor(
    public global:GlobalService,
    private stripe: Stripe,
    private route:Router,
    public apiService:ApiService,
    public store:InAppPurchase2,
    public navCtrl:NavController
  ) { }

  ngOnInit() {
    this.showLoading = true;
    this.fetch_subscriptionList('showskeleton','');
  }
  
  public setSelectedData_default() {
    this.selectedItemDetails = this.subscriptionList[this.selectedIndex];
  }

  public fetch_subscriptionList(value:any,event:any){
    this.apiService.api_fetchSubscriptions()
    .then((response:any)=>{
      console.log("response :",response);
      if(value == 'showskeleton'){this.showLoading = false;}else{event.target.complete();}
      if(response.status == "success"){
        if(response.data){
          let data = response.data;
          if(data.plans){
            this.selectedIndex = 0;
            this.subscriptionList = data.plans
            this.setSelectedData_default();
          }else{this.subscriptionList = []}
          
          
        }
      }else{
        this.subscriptionList=[]
        this.global.presentToast(response.message,null);
      }
    })
    .catch(()=>{
      this.subscriptionList=[]
      // if(value == 'showskeleton'){this.showLoading = false;}
      if(value == 'showskeleton'){this.showLoading = false;}else{event.target.complete();}
    })
    
  }

  handleRefresh(event:any){
    this.fetch_subscriptionList('',event);
  }

  selectedPackage(index:any,item:any){
    this.selectedIndex = index;
    this.setSelectedData_default();
  }

  async buyPlan(){
    // if(this.global.platform.is('ios')){
    //   this.buyinAppPurchanses()
    // }else{

      console.log('buyPlan :',this.selectedItemDetails);
      const modal = await this.global.modalController.create({
        component: PaymentScreenPage,
        cssClass: 'my-custom-class',
        componentProps: {
          'plan_details': JSON.stringify(this.selectedItemDetails)
        }
      });
      modal.onDidDismiss().then((data:any) => {
        console.log('dismissed', data);
        if(data?.data == false){

        }else{
          this.navCtrl.navigateRoot(['./home-page']);
        }
      });
      modal.addEventListener('ionModalWillDismiss', (event: any) => {
        console.log('works');
      });
      return await modal.present();

    // }


    
  }

  /* ========= Inapp purchases configuration function ========= */
  public configureStart(){
    this.global.platform.ready()
    .then(()=>{
      // console.log(" .... Platform is Ready now ......");
        // this.store.verbosity = this.store.DEBUG;
        this.store.register({
          id: "ponctuel_10_1m",
          // id:this.ios_plan_name,
          type: this.store.CONSUMABLE,
        });
        this.store.when("ponctuel_10_1m")
        // this.store.when(this.ios_plan_name)
          .approved((p:any) => {
            // this.global.dynamictimeToastHide();
            console.log("p.approved : ",JSON.stringify(p));
            p.verify()
          })
          .verified((p:any) => {
            console.log("p.verified : ",JSON.stringify(p));
            p.finish()
          })
          .finished((p:any) =>{
            // alert("Order finished : "+ JSON.stringify(p));
            if(p.transaction){
              if(p.transaction.id){
                alert("Order finished : "+ JSON.stringify(p.transaction.id));
                // this.navController.navigateRoot(["/user-type-category"]);
                // this.startSubmittingResponseData_forAppleInappPurchases(p)
              }
            }
          })
          .cancelled((p:any) =>{
            // this.global.dynamictimeToastHide();
            //message == > Payment canceled by the user
            // alert("Paiement annulé par l'utilisateur")
            this.global.presentToast("cancelled" + JSON.stringify(p),null)
          });
        this.store.refresh();

    })
    .catch((error)=>{
      console.log(" .... Platform is not ready");
    })
  }


    /* === Buy Inapp Purchases ===== */ 
    buyinAppPurchanses(){
      this.global.presentToast("Processing in progress, please wait...",null)
      // this.store.order(this.ios_plan_name)
      this.store.order("ponctuel_10_1m")
      .then((success:any)=>{
        console.log("order success  : ", JSON.stringify(success))
      })
      .catch((error:any)=>{
        // this.global.dynamictimeToastHide();
        console.log("Order error : ", JSON.stringify(error));
      })
    }
      

//       STRIPE_KEY=pk_test_51J3baoSBUUFJKWGU9pW4G46U6JXYLSU4H3YSUI7HmEDe0gqjjUU2EmIp9E4hitZRoqzdXDFkNBpi5RcunqQKQqOV002a7YRnWC
// STRIPE_SECRET=sk_test_51J3baoSBUUFJKWGUFlgrMf167DHmgSqzJvHqSkOwBRymOnvwjkt9Nns0tCkPB4aOuXeyit5xboLLULE9CpkUQ2Uk00OzqfYJG1

}
