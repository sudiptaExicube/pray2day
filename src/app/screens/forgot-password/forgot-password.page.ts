import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, ModalController, Platform } from '@ionic/angular';
import { ApiService } from 'src/app/api.service';
import { GlobalService } from 'src/app/global.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  public passwordType: string = 'password';
  public passwordIcon: string = 'eye-off';

  public con_passwordType: string = 'password';
  public con_passwordIcon: string = 'eye-off';

  public email:any = "";
  
  public page:any = "changePsw"

  constructor(
    public global:GlobalService,
    private modalController: ModalController,
    private platform: Platform,
    public alertController: AlertController,
    public apiService: ApiService,
    private route: Router,
  ) { }

  ngOnInit() {
  }


  public changePassword(){
    if(!this.email.trim()){
      this.global.showToast('Please enter email')
    }else if(!this.global.isvalidEmailFormat(this.email)){
      this.global.showToast('Ivalid email format')
    }else{
      this.callForgotpasswordFunction()
    }
  }

  public callForgotpasswordFunction(){
    this.global.presentLoadingDefault();
    let params={
      email:this.email
    }
    this.apiService.forgotPasswordFunction(params)
    .then((success:any)=>{
      this.global.presentLoadingClose();
      console.log("forgot password : ",success);
      if(success.status == 'success'){
        // this.global.presentToast(success.message, 5000)
      }else if(success.status == 'otpverification'){
        
        let successdata:any = success.data; 
        this.global.user_token = successdata.otp_token;
        console.log("Forgot password : ", this.global.user_token);
        this.global.passwordReset_type = 'user-resetpassword';
        this.route.navigate(['./reset-password'])
      }else{
        this.global.presentToast(success.message,null);
      }
    })
    .catch((error:any)=>{
      this.global.presentLoadingClose()
      this.global.presentToast(JSON.stringify(error),null);
    })
  }
}



