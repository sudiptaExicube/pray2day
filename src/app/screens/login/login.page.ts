import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ApiService } from 'src/app/api.service';
import { GlobalService } from 'src/app/global.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public email: any = "pramit.paul@yopmail.com";
  public password: any = "12345678";
  // public email: any = "";
  // public password: any = "";

  public passwordType: string = 'password';
  public passwordIcon: string = 'eye-off';
  public remember_me: boolean = false;

  constructor(
    private route: Router,
    public global:GlobalService,
    public apiService:ApiService,
    public navCtrl:NavController
  ) { }


  ngOnInit() {
  }
  // ============ PASSWORD SHOW HIDE FUNCTIONLITY ============
  public hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  public goForgotPasw() {
    this.route.navigate(['./forgot-password'])
  }

  public login(){
    if(!this.email.trim()){
      this.global.showToast('Please enter email')
    }else if(!this.global.isvalidEmailFormat(this.email)){
      this.global.showToast('Ivalid email format')
    }else if(!this.password.trim()){
      this.global.showToast('Please enter password')
    }else {
      // this.route.navigate(['./home-page']);
      this.startLogin()
    }
    
   // this.route.navigate(['./intro'])
  }

  public startLogin(){
      this.global.presentLoadingDefault();
      let data={
        email:this.email,
        password:this.password,
        // fcm_token:this.global.deviceToken ? this.global.deviceToken :""
      }
      this.apiService.signinFunction(data)
      .then((success:any)=>{
        console.log("success : ", success);
        // this.global.presentLoadingClose()
        if(success.status == 'success'){
          if(success.data){
            let successdata:any = success.data; 
            localStorage.setItem("access_token",successdata.token.access_token)
            localStorage.setItem("user_details",JSON.stringify(successdata.user))
            this.global.userdetails = successdata.user;
            this.global.user_token = successdata.token.access_token;
            // this.navCtrl.navigateRoot(['./home-page']);
            this.fetch_my_subscription();
          }
        }else if(success.status == "otpverification"){
          this.global.presentLoadingClose();
          // let navigationExtras: NavigationExtras = {
          //   queryParams: {
          //     otp_token:success?.data?.otp_token,
          //     from:"login"
          //   }
          // }
          // this.navCtrl.navigateRoot(['./otp-screen'],navigationExtras);
          // this.global.presentToast(success.message);
        }else{
          this.global.presentLoadingClose();
          this.global.presentToast(success.message,null);
        }
        console.log("Login success is  : ", success)
      })
      .catch((error:any)=>{
        this.global.presentLoadingClose()
        console.log("Login error : ", error)
      })


  }
  
  public gotoRegistration() {
    this.route.navigate(['./registration'])
  }



  public fetch_my_subscription(){
    this.apiService.api_mySubscription()
    .then((response:any) => {
      this.global.presentLoadingClose();
      console.log("response :", response);
      if(response.status == "success") {
        let data = response.data;

        this.navCtrl.navigateRoot(['./home-page']);
        if(data?.subscription){
          this.global.subscriptionDetails = data?.subscription;

          console.log("this.global.subscriptionDetails : ", this.global.subscriptionDetails)
          // this.global.subscriptionDetails = null
        }else{
          this.global.subscriptionDetails = null;
          console.log("this.global.subscriptionDetails not : ", this.global.subscriptionDetails)
          this.navCtrl.navigateRoot(['./subscription']);
        }
      }else{
        this.global.subscriptionDetails = null;
        this.navCtrl.navigateRoot(['./subscription']);
      }
    }).
    catch((error:any)=>{ 
      this.global.presentLoadingClose()
      console.log("error :", error); })
    }

}
