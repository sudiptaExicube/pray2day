import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ApiService } from 'src/app/api.service';
import { GlobalService } from 'src/app/global.service';

@Component({
  selector: 'app-payment-screen',
  templateUrl: './payment-screen.page.html',
  styleUrls: ['./payment-screen.page.scss'],
})
export class PaymentScreenPage implements OnInit {

  @Input() plan_details: any;

  public password:any="";
  public con_passwordType: string = 'password';
  public con_passwordIcon: string = 'eye-off';


  public card_name:any="";
  public card_number:any = "";
  public card_month:any="";
  public card_year:any="";
  public card_cvv:any="";
  // public card_country:any = 233;
  public card_country:any = null;
  public card_state:any=null;
  public card_zip:any="";

  public card_city:any="";
  public card_addredd1:any="";
  public card_addredd2:any="";

  public countryLists:any= [];
  public current_plan:any;
  public stateLists:any=[];

  constructor(
    public global:GlobalService,
    public apiService:ApiService,
    public modalCtrl:ModalController
  ) { }

  ngOnInit() {
    console.log("plan_details :" , this.plan_details);
    if(this.plan_details){
      this.current_plan = JSON.parse(this.plan_details);
    }

    if(this.global.countryList.length == 0){
      this.global.presentLoadingDefault();
      this.fetchCountryList();
    }else{
      this.setcountryValue();
    }
    console.log("sdbcksdbcsdkbkjsdbcs : ", this.global.countryList);
  }

  changeCountry(){
    this.global.presentLoadingDefault();
    this.card_state = null;
    this.fetchStateLists()
  }
  
  public setcountryValue(){
    this.countryLists = this.global.countryList;
    // this.global.presentLoadingDefault();
    // this.fetchStateLists()
  }

  public fetchCountryList() {
    this.apiService.api_fetchCountries()
    .then((response:any) => {
      this.global.presentLoadingClose();
      console.log("response :", response);
      if(response.status == "success") {
        let data = response.data
        if(data?.countries){
          this.global.countryList = data?.countries;
          setTimeout(() => {
            this.setcountryValue();
          }, 500);
        }else{this.global.countryList = []}
      }else{
        this.global.countryList = []
      }
    }).
    catch((error:any)=>{ 
      this.global.presentLoadingClose();
      console.log("error :", error); })
  }

  public fetchStateLists(){
    let params={
      country_id:this.card_country
    }
    this.apiService.api_fetchStates(params)
    .then((response:any) => {
      this.global.presentLoadingClose();
      console.log("response :", response);
      if(response.status == "success") {
        let data = response.data
        if(data?.states){
          // this.global.stateLists = data?.states;
          this.stateLists = data?.states;
          setTimeout(() => {
            this.setcountryValue();
          }, 500);
        }else{
          // this.global.stateLists = [],
          this.stateLists = []
        }
      }else{
          // this.global.stateLists = [],
          this.stateLists = []
      }
    }).
    catch((error:any)=>{ 
      this.global.presentLoadingClose();
      console.log("error :", error); })
  }

  public closeMe(){
    this.global.modalController.dismiss();
  }
  public close_icon_click(){
    this.global.modalController.dismiss(false);
  }

  hideShowPassword(err:any){ }

  public callStriptPayment(){
    if(!this.card_name){
      this.global.presentToast("Name is required",null)
    }else if(!this.card_number){
      this.global.presentToast("Card number is required",null)
    }else if(!this.card_month){
      this.global.presentToast("Month is required",null)
    }else if(!this.card_year){
      this.global.presentToast("Year is required",null)
    }else if(!this.card_cvv){
      this.global.presentToast("Month is required",null)
    }else if(!this.card_country){
      this.global.presentToast("Country is required",null)
    }else if (!this.card_state){
      this.global.presentToast("State is required",null)
    }else if (!this.card_city){
      this.global.presentToast("City is required",null)
    }else if (!this.card_addredd1){
      this.global.presentToast("Address is required",null)
    }else if(!this.card_zip){
      this.global.presentToast("Zip code is required",null)
    }else{
      let cardNum:any = this.card_number;
      let cardMon:any = this.card_month;
      let cardYr:any = this.card_year;
      let cardCv:any = this.card_cvv;
      if(cardNum.toString().length <= 15){
        this.global.presentToast("Card number is required",null)
      }else if(cardMon.toString().length <= 1){
        this.global.presentToast("Month is required",null)
      }else if(cardYr.toString().length <= 1){
        this.global.presentToast("Year is required",null)
      }else if(cardCv.toString().length <= 2){
        this.global.presentToast("CVV is required",null)
      }else{
        console.log("working")
        this.function_stripeApi()
      }
    }
  }

  public function_stripeApi(){
    this.global.presentLoadingDefault()
    let params={
      payment_mode:'stripe',
      plan_id:this.current_plan?.id,
      card_no:this.card_number,
      card_exp_month:this.card_month,
      card_exp_year:this.card_year,
      card_cvc:this.card_cvv,
      address_country_id:this.card_country,
      address_zip:this.card_zip,
      address_state_id:this.card_state,
      address_line1:this.card_addredd1,
      address_line2:this.card_addredd2,
      address_city:this.card_city
    }

    this.apiService.api_stripePayment(params)
    .then((success:any)=>{
      // this.global.presentLoadingClose()
      console.log("payment : ", success)
      if(success.status == "success"){
        this.global.presentToast(success.message,null);
        // this.closeMe()
        this.fetch_my_subscription();

      }else{
        this.global.presentLoadingClose()
        this.global.presentToast(success.message,null);
        // this.closeMe();
        //this.global.modalController.dismiss(false);
      }
    })
    .catch((error:any)=>{
      this.global.presentLoadingClose();
      // this.closeMe()
      this.global.modalController.dismiss(false);
      console.log("payment error : ", error)
      this.global.presentToast(JSON.stringify(error),null);
    })
  }


  public fetch_my_subscription(){
    this.apiService.api_mySubscription()
    .then((response:any) => {
      console.log("response :", response);
      this.global.presentLoadingClose()
      this.closeMe()
      if(response.status == "success") {
        let data = response.data;
        if(data?.subscription){
          this.global.subscriptionDetails = data?.subscription;

          console.log("this.global.subscriptionDetails : ", this.global.subscriptionDetails)
          // this.global.subscriptionDetails = null
        }else{
          this.global.subscriptionDetails = null;
          console.log("this.global.subscriptionDetails not : ", this.global.subscriptionDetails)
        }
      }else{
        this.global.subscriptionDetails = null;
      }
    }).
    catch((error:any)=>{ 
      console.log("error :", error); })
    }

}
