import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaymentScreenPageRoutingModule } from './payment-screen-routing.module';

import { PaymentScreenPage } from './payment-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaymentScreenPageRoutingModule
  ],
  declarations: [PaymentScreenPage]
})
export class PaymentScreenPageModule {}
