import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { GlobalService } from 'src/app/global.service';

@Component({
  selector: 'app-privicy',
  templateUrl: './privicy.page.html',
  styleUrls: ['./privicy.page.scss'],
})
export class PrivicyPage implements OnInit {

  public showSkleton:boolean = false;
  public privacy:any = null;

  constructor(
    public global:GlobalService,
    public apiservice:ApiService
  ) { }

  ngOnInit() {
    if(this.global.privacy){
      this.privacy = this.global.privacy;
    }else{
      this.showSkleton = true
      this.fetch_privacy()
    }
  }

  public fetch_privacy(){
    let params={
      slug:'privacy-policy'
    }
    this.apiservice.api_fetchCMS('cms/contents',params)
    .then((success:any)=>{
      console.log("success :",success)
      if(success.data) {
        let data = success.data;
        if(data?.cmscontent){
          this.global.privacy = data?.cmscontent
          this.privacy = data?.cmscontent
        }

      }
    })
    .catch((error:any)=>{
      console.log("error :",error)
      
    })
  }


  handleRefresh(event:any) {
    let params={
      slug:'privacy-policy'
    }
    this.apiservice.api_fetchCMS('cms/contents',params)
    .then((success:any)=>{
      event.target.complete();
      this.showSkleton = false
      if(success.data) {
        let data = success.data;
        if(data?.cmscontent){
          this.global.privacy = data?.cmscontent
          this.privacy = data?.cmscontent
        }

      }
    })
    .catch((error:any)=>{
      event.target.complete();
      this.showSkleton = false
      
    })
  }



}
