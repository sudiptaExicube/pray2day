import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PrivicyPageRoutingModule } from './privicy-routing.module';

import { PrivicyPage } from './privicy.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PrivicyPageRoutingModule
  ],
  declarations: [PrivicyPage]
})
export class PrivicyPageModule {}
