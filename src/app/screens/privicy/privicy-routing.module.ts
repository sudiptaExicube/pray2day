import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PrivicyPage } from './privicy.page';

const routes: Routes = [
  {
    path: '',
    component: PrivicyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrivicyPageRoutingModule {}
