import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { ApiService } from 'src/app/api.service';
import { GlobalService } from 'src/app/global.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {
 public name :any = "";
  public password: any = "";
  public email: any = "";

  public passwordType: string = 'password';
  public passwordIcon: string = 'eye-off';
  public remember_me: boolean = false;
  public religion = "";
  public ageGroup = "";

  public religions:any;
  public age_groups:any;

  public customActionSheetOptions: any;
  constructor(
    public global:GlobalService,
    private route: Router,
		public loadingCtrl: LoadingController,
		public alertController: AlertController,
    public navCtrl: NavController,
    public apiService:ApiService

  ) {
    this.customActionSheetOptions = {
      mode: "md",
      cssClass: 'remove-cancelBtn'
    };
   }

   ngOnInit() {
    if(!this.global.app_globalData){
      this.apiService.GlobalDataFunction(true)
    }
    // else{
    //   if(this.global.app_globalData.age_groups){
    //     this.age_groups = this.global.app_globalData.age_groups
    //   }
    //   if(this.global.app_globalData.religions){
    //     this.religions = this.global.app_globalData.religions
    //   }
    // }
   }

   // ============ PASSWORD SHOW HIDE FUNCTIONLITY ============
   public hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  public goForgotPasw() {}

  public login(){}
  
  public gotoRegistration() {
    if(!this.name.trim()){
      this.global.showToast('Please enter name')
    }else if(!this.email.trim()){
      this.global.showToast('Please enter email')
    }else if(!this.global.isvalidEmailFormat(this.email)){
      this.global.showToast('Ivalid email format')
    }else if(!this.religion){
      this.global.showToast('Please select your religion')
    }else if(!this.ageGroup){
      this.global.showToast('Please select age group')
    }else if(!this.password.trim()){
      this.global.showToast('Please enter password')
    }else if(this.password.length < 6){
      this.global.showToast('Password length mustbe 6 digit')
    }else{
      // this.route.navigate(['./otp-verification'])
      this.startRegistration()
    }
    console.log("this.religion : ", this.religion)
    
  }

  public startRegistration(){
    this.global.presentLoadingDefault();
    let data={
      name:this.name,
      email:this.email,
      religion_id:this.religion,
      age_group_id:this.ageGroup,
      password:this.password,
      password_confirmation:this.password
      // fcm_token:this.global.deviceToken ? this.global.deviceToken :""
    }
    this.apiService.registrationFunction(data)
    .then((success:any)=>{
      this.global.presentLoadingClose()
      console.log("success : ", success);
      if(success.status == 'success'){
        // if(success.data){
        //   let successdata:any = success.data; 
        //   this.global.user_token = successdata.otp_token;
        //   this.route.navigate(['./otp-verification'])
        // }
      } else if (success.status == 'otpverification'){
        if(success.data){
          let successdata:any = success.data; 
          this.global.user_token = successdata.otp_token;
          console.log("registration : ", this.global.user_token);
          this.global.passwordReset_type = 'user-register';
          this.route.navigate(['./otp-verification'])
        }
      }else{
        this.global.presentToast(success.message,null);
      }
      console.log("Login success is  : ", success)
    })
    .catch((error:any)=>{
      this.global.presentLoadingClose()
      console.log("Login error : ", error)
    })


  }

  back(){
    this.navCtrl.pop()
  }

}
