import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PrayerSubpagePageRoutingModule } from './prayer-subpage-routing.module';

import { PrayerSubpagePage } from './prayer-subpage.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PrayerSubpagePageRoutingModule
  ],
  declarations: [PrayerSubpagePage]
})
export class PrayerSubpagePageModule {}
