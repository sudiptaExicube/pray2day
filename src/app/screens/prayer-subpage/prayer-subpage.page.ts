import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';
import { GlobalService } from 'src/app/global.service';

@Component({
  selector: 'app-prayer-subpage',
  templateUrl: './prayer-subpage.page.html',
  styleUrls: ['./prayer-subpage.page.scss'],
})
export class PrayerSubpagePage implements OnInit {
  public quickActionType:string="";
  public pageNumberEntry:string = "";

  public showLoading:boolean = false;
  public prayer_id:any;
  public prayerDetails:any={};
  public chaptarDetails:any = [];


  public audioPlayer1:any;
  isAudioPlaying: boolean = false;
  audioSource: any = ""

  constructor(
    public global:GlobalService,
    private route: Router,
    private activatedRoute:ActivatedRoute,
    private router: Router,
    private apiService: ApiService
  ) { 
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params:any) => {
      this.quickActionType="";
      this.pageNumberEntry = ""
      if (params ) {
        console.log("params : ", params);
        if(params.prayer_id){
          this.prayer_id = params.prayer_id;
          this.fetch_prayerChapter();
        }
      }
    })
  }

  toggleAudio() {
    this.audioPlayer1 = document.getElementById('audioPlayer1') as HTMLAudioElement;

    if (this.isAudioPlaying) {
      this.audioPlayer1.pause();
      this.audioPlayer1.currentTime = 0;
      this.audioPlayer1.src = '';
    } else {
      this.audioPlayer1.play();
    }

    this.isAudioPlaying = !this.isAudioPlaying;
  }

  checkAudioStatus(){
    // this.audioPlayer1 = document.getElementById('audioPlayer') as HTMLAudioElement;
    if(this.audioPlayer1){
      this.audioPlayer1.pause();
      this.audioPlayer1.currentTime = 0;
      this.isAudioPlaying = false;
      this.audioSource = null
    }
  }

  public fetch_prayerChapter(){
    this.showLoading = true;
    let params:any={
      prayer_id:this.prayer_id
    }
    console.log("params dc : ", params);
    // fetchPrayerChapter
    this.apiService.fetchPrayerChapter(params)
    .then((response:any)=>{
      console.log("response :", response)
      this.showLoading = false;
      if(response.status == "success"){
        if(response.data){
          let data = response.data;
          // this.homeData = response.data
          if(data.prayer){
            this.prayerDetails = data.prayer
            if(this.prayerDetails?.sound_file){
              this.audioSource = this.prayerDetails?.sound_file;
            }
          }
          if(data.chapters){
            this.chaptarDetails = data.chapters
          }
        }
      }else{
        this.global.presentToast(response.message,null);
      }
    })
    .catch(()=>{
      this.showLoading = false;
    })

  }

  noType(ev:any){
    this.checkAudioStatus();
    console.log(ev)
    if(this.pageNumberEntry != '' && !isNaN(parseInt(this.pageNumberEntry))){
      this.quickActionType="number"
    }else{
      this.quickActionType=""
    }
   
  }

  startReading(value:any){
    this.checkAudioStatus();

    if(value.prayer_lines_count == 0){
      this.global.presentToast("No prayer available in this chapter",null)
    }else{
      const navigationExtras: NavigationExtras = {
        queryParams: {
          //chaptarDetails:JSON.stringify(this.chaptarDetails),
          chapter_id:value.id
        }
      };
      // console.log("chapterID: ", value.id);
      this.route.navigate(['./prayer-view'],navigationExtras)
    }
    // if(value <= this.chaptarDetails){
    //   console.log("Matching ................");
    // }else{
    //   console.log("not matching ................");
    // }

    console.log(this.chaptarDetails);

  }


  public goToSpecificIndex(){

    if(this.quickActionType  == ""){
      if(this.chaptarDetails[0].prayer_lines_count == 0){
        this.global.presentToast("No prayer available in this chapter",null)
      }else{
        let data={ id:this.chaptarDetails[0].id }
        this.startReading(data);
      }
    }else{


      let found:boolean = false;
      for(let i=0;i<this.chaptarDetails.length;i++){
        // if(this.chaptarDetails[i].id == this.pageNumberEntry){   //This line is disable
        if(this.chaptarDetails[i].serial == this.pageNumberEntry){
          if(this.chaptarDetails[i].prayer_lines_count == 0){
            found = true;
          }
        }
      }
      if(found){
        this.global.presentToast("No prayer available in this chapter",null)
      }else{
        if(parseInt(this.pageNumberEntry) <= this.chaptarDetails.length && parseInt(this.pageNumberEntry) > 0){
          // let value = parseInt(this.pageNumberEntry); //This line is disable
          let index = parseInt(this.pageNumberEntry);   //New line added
          let actIndex = index-1;                       //New line added
          console.log("index value : ", index)          //New line added
          let value = this.chaptarDetails[actIndex].id; //New line added

          let data={ id:value }
          this.startReading(data);
        }else{
          console.log("csjbcskdjcbdsjkcbskjcbskcbdkds")
        }
      }


    }

  }

}
