import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PrayerSubpagePage } from './prayer-subpage.page';

const routes: Routes = [
  {
    path: '',
    component: PrayerSubpagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrayerSubpagePageRoutingModule {}
