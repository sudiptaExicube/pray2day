import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ApiService } from 'src/app/api.service';
import { GlobalService } from 'src/app/global.service';
// import { Camera, CameraResultType } from '@capacitor/camera';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
import { Capacitor } from '@capacitor/core';
import { Camera, CameraOptions } from '@awesome-cordova-plugins/camera/ngx';


@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.page.html',
  styleUrls: ['./editprofile.page.scss'],
})
export class EditprofilePage implements OnInit {
 public name :any = "John Doe";
  public password: any = "";
  public email: any = "john@gmail.com";

  public passwordType: string = 'password';
  public passwordIcon: string = 'eye-off';
  public remember_me: boolean = false;
  public religion = "jewish";
  public ageGroup = "18-60";

  public customActionSheetOptions: any;
  constructor(
    public navCtrl: NavController,
    public global:GlobalService,
    private route: Router,
    private apiService: ApiService,
    public camera:Camera,
  ) {
    this.customActionSheetOptions = {
      mode: "md",
      cssClass: 'remove-cancelBtn'
    };
   }

   ngOnInit() {
    // global?.userdetails?.avatar

    if(!this.global.app_globalData){
      this.apiService.GlobalDataFunction(true)
    }

    if(this.global.userdetails){
        console.log(this.global.userdetails);
        this.name = this.global.userdetails.name;
        this.email = this.global.userdetails.email;
        this.religion = this.global.userdetails.religion.id;
        this.ageGroup = this.global.userdetails.agegroup.id;
    }

   }

   // ============ PASSWORD SHOW HIDE FUNCTIONLITY ============
   public hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  public goForgotPasw() {}

  public login(){}
  public gotoRegistration() {
    
  }

  back(){
    this.navCtrl.pop()
  }

  gotoChangePassword(){
    this.route.navigate(['./change-password']);
    
  }
  // updateProfileFunction
  public updateProfile(){
    this.global.zone.run(()=>{
      this.global.presentLoadingDefault();
    });
    let params={
      type:'basic-details',
      name:this.name,
      email:this.email,
      religion_id:this.religion,
      age_group_id:this.ageGroup
    }

    this.apiService.apiWithTokenBody('user/profile/update',params)
    .then((success:any)=>{
      this.global.zone.run(()=>{
        this.global.presentLoadingClose()
      });
      console.log("success : ", success);
      if(success.status == 'success'){
        if(success.data){
          let successdata:any = success.data; 
          // localStorage.setItem("access_token",successdata.token.access_token)
          localStorage.setItem("user_details",JSON.stringify(successdata.user))
          this.global.userdetails = successdata.user;
          // this.global.user_token = successdata.token.access_token;
          // this.navCtrl.navigateRoot(['./home-page']);
        }
      }else if(success.status == "otpverification"){

      }else{
        this.global.presentToast(success.message,null);
        this.name = this.global.userdetails.name;
        this.email = this.global.userdetails.email;
        this.religion = this.global.userdetails.religion.id;
        this.ageGroup = this.global.userdetails.agegroup.id;
      }
    })
    .catch((error:any)=>{
      this.global.zone.run(()=>{
        this.global.presentLoadingClose();
      });
      this.name = this.global.userdetails.name;
      this.email = this.global.userdetails.email;
      this.religion = this.global.userdetails.religion.id;
      this.ageGroup = this.global.userdetails.agegroup.id;
    })

  }





  b64toBlob(b64Data:any, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    //console.log(blob);
    return blob;
  }

  imageUrl:any=null;
  public async uploadProfileImg(){
    /*
    // const takePicture = async () => {
      const image = await Camera.getPhoto({
        quality: 90,
        allowEditing: true,
        resultType: CameraResultType.Base64
      });
    
      // image.webPath will contain a path that can be set as an image src.
      // You can access the original file using image.path, which can be
      // passed to the Filesystem API to read the raw data of the image,
      // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
      // this.imageUrl = image.base64String;
      var fileUri:any = image.base64String;

      this.imageUrl = 'data:image/jpg;base64,' + fileUri;
        const blobData = this.b64toBlob(fileUri, `image/jpeg`);
        this.profileImage(blobData);
      console.log("imageUrl : ", blobData)



    // let params={
    //   type:'profile-picture',
    //   imagePath:this.imageUrl
    // }

    //   this.apiService.uploadProfileImage('user/profile/update',params)
    //   .then((success:any)=>{
    //     console.log("profile API response success : ", success)
    //   })
    //   .catch((error:any)=>{
    //     console.log("profile API response error : ", error)
    //   })

    */

    const cameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      correctOrientation: true,
    };
    this.camera.getPicture(cameraOptions).then(
      (fileUri) => {
        this.imageUrl = 'data:image/jpg;base64,' + fileUri;
        const blobData = this.b64toBlob(fileUri, `image/jpeg`);
        this.profileImage(blobData);
      },
      (err) => {}
    );

  }

  profileImage(blobdata:any) {
    // this.global.loadingOpen();
    // const fd = new FormData();
    // fd.append('type', 'profile-picture');
    // fd.append('profile_picture', blobdata, 'avatar.jpeg');
    // fd.append('module_name', 'user');
    // fd.append('respective_id', this.global.userdetails.id);
    //console.log(blobdata)
    // this.apiService.uploadFile(fd)
    // .then((res: any) => {
    //   // this.global.loadingClose();
    //   //console.log(res)
    //   // if(res.status == 200){
    //   //   this.global.toast_msg(res.message);
    //   //   this.getProfile(null)
    //   // }else{
    //   //   this.global.toast_msg(res.message);
    //   //   this.getProfile(null)
    //   // }
      
    // });
    this.global.presentLoadingDefault();
    let params={
      type:'profile-picture',
      imagePath:blobdata
    }

      this.apiService.uploadProfileImage('user/profile/update',params)
      .then((success:any)=>{
        this.global.presentLoadingClose();
        console.log("profile API response success : ", success)
        if(success.status == 'success'){
          this.global.presentToast(success.message,null)
          let successdata:any = success.data; 
          localStorage.setItem("user_details",JSON.stringify(successdata.user))
          this.global.userdetails = successdata.user;

        }else{
          this.global.presentToast(success.message,null)
        }
      })
      .catch((error:any)=>{
        this.global.presentLoadingClose();
        console.log("profile API response error : ", error)
        this.global.presentToast(JSON.stringify(error),null)
      })

  }

}

