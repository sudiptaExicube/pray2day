import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController, NavController, Platform } from '@ionic/angular';
import { ApiService } from 'src/app/api.service';
import { GlobalService } from 'src/app/global.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {
  public passwordType: string = 'password';
  public passwordIcon: string = 'eye-off';

  public con_passwordType: string = 'password';
  public con_passwordIcon: string = 'eye-off';

  public con_passwordType2: string = 'password';
  public con_passwordIcon2: string = 'eye-off';

  public password:any = "";
  public confirmPassword:any = "";
  public old_password:any = "";
  public page:any = "changePsw"

  constructor(
    public global:GlobalService,
    private modalController: ModalController,
    private platform: Platform,
    public alertController: AlertController,
    public apiService:ApiService,
    public navCtrl:NavController
  ) { }

  ngOnInit() {
  }

  public hideShowPassword(type:string) {
    if (type == 'password') {
      this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
      this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
      //console.log(this.passwordType)
      //console.log(this.passwordIcon)
    }else if(type == 'changePassword'){
      this.con_passwordType2 = this.con_passwordType2 === 'text' ? 'password' : 'text';
      this.con_passwordIcon2 = this.con_passwordIcon2 === 'eye-off' ? 'eye' : 'eye-off';
    } else {
      this.con_passwordType = this.con_passwordType === 'text' ? 'password' : 'text';
      this.con_passwordIcon = this.con_passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
      //console.log(this.con_passwordType)
      //console.log(this.con_passwordIcon)
    }
  }

  

  updatePassword(){
    if (!this.old_password.trim()) {
      this.global.presentToast("Password cannot be blank",null)
    } else if (!this.password.trim()) {
      this.global.presentToast("New password cannot be blank",null)
    }else if (!this.confirmPassword.trim()) {
      this.global.presentToast("Confirm password cannot be blank",null)
    } else{
      if(this.password == this.confirmPassword){
        this.alertController.create({
          header: 'Confirmation',
          message: 'Are you sure, you want to update your password?',
          // message: 'On password change , account will automatically logged out',
          backdropDismiss: false,
          buttons: [{
            text: 'No',
            role: 'cancel',
            handler: () => { }
          }, {
            text: 'Change Password',
            handler: () => {
              this.changePassword();
            }
          }]
        })
          .then(alert => {
            alert.present();
          });
      }else{
        this.global.presentToast("New password and Confirm password should be same",null)
      }
    }
  }

  public changePassword(){
    let params={
      type:'password',
      current_password:this.old_password,
      new_password:this.password,
      new_password_confirmation:this.confirmPassword
    }
    this.global.presentLoadingDefault();
    this.apiService.apiWithTokenBody('user/profile/update',params)
    .then((success:any)=>{
      this.global.presentLoadingClose()
      // console.log("success : ", success);
      if(success.status == 'success'){
        if(success.data){
          let successdata:any = success.data; 
          this.global.presentToast(success.message,null);
          this.navCtrl.pop()
        }
      }else if(success.status == "otpverification"){

      }else{
        this.global.presentToast(success.message,null);
      }
    })
    .catch((error:any)=>{
      this.global.presentLoadingClose()
    })


  }

  close(){
    this.modalController.dismiss();
  }

  exitApp() {
    this.platform.backButton.subscribe(() => {
      if (this.page== "changePsw") {
        this.modalController.dismiss();
      }
    });
  }
}



