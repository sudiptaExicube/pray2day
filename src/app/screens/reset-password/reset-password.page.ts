import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ApiService } from 'src/app/api.service';
import { GlobalService } from 'src/app/global.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit {

    public enableResend: boolean = false;
    public hideTime = 0;
    public mobile_no: any = '';
    public otp: any = {
      input1: '',
      input2: '',
      input3: '',
      input4: '',
    };
    public otpTimer: any = "0";
    public from: any = "";
    public paramData: any = "";

    password:any;
    confirmPassword:any;
    otpValue:any;

    public passwordType: string = 'password';
    public passwordIcon: string = 'eye-off';
    public passwordType2: string = 'password';
    public passwordIcon2: string = 'eye-off';
  
    constructor(
      public global:GlobalService,
      private route: Router,
      private apiService: ApiService,
      private navCtrl:NavController
    ) { }
  
    ngOnInit() {
    }

    public hideShowPassword() {
      this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
      this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }
    public hideShowConfirmPassword() {
      this.passwordType2 = this.passwordType2 === 'text' ? 'password' : 'text';
      this.passwordIcon2 = this.passwordIcon2 === 'eye-off' ? 'eye' : 'eye-off';
    }
  
    /* === FORWARD TO NEXT FIELD === */
    moveFocus(next: any, event: any, prev: any) {
      console.log(event, prev);
      if (event.key == 'Backspace') {
        prev.setFocus();
      } else {
        next.setFocus();
      }
    }
  
  
    /* === Submit OTP === */
    public otpVerification(){

      if(!this.password.trim()){
        this.global.presentToast("Password cannot be empty",null)
      }else if (!this.confirmPassword.trim()){
        this.global.presentToast("Confirm Password cannot be empty",null)
      }else if(!this.otpValue.trim()){
        this.global.presentToast("OTP cannot be empty",null)
      }else{
        if(this.password.trim() ==this.confirmPassword.trim()){
          this.startResetPassword()
        }else{
          this.global.presentToast("Password & Confirm password should be same",null)
        }        
      }
      //this.route.navigate(['./home-page']);
    }

    public startResetPassword(){
      this.global.presentLoadingDefault()
      let params = {
        // type:'user-register',
        type:this.global.passwordReset_type,
        verification_token:this.global.user_token,
        otp:this.otpValue,
        new_password:this.password,
        new_password_confirmation:this.confirmPassword
      }
      console.log("OTP verification data : ",params)
  
      this.apiService.otpVerificationFunction(params)
      .then((success:any)=>{
        this.global.presentLoadingClose();
        console.log("success.message : ",success.message)
        if(success.status == 'success'){
          this.global.presentToast(success.message, null);
          this.global.passwordReset_type = ''
          this.navCtrl.navigateRoot(['login'])
        }else{
          this.global.presentToast(success.message,null);
        }
      })
      .catch((error:any)=>{
        this.global.presentLoadingClose()
        this.global.presentToast(JSON.stringify(error),null);
      })
    }

  
    /* === RESEND OTP === */
    public resendOtp(){
      this.global.presentLoadingDefault()
      let params={
        verification_token:this.global.user_token
      }
      this.apiService.resendOtpFunction(params)
      .then((success:any)=>{
        this.global.presentLoadingClose()
        if(success.status == 'success'){
          this.global.presentToast(success.message, null)
        }else{
          this.global.presentToast(success.message,null);
        }
      })
      .catch((error:any)=>{
        this.global.presentLoadingClose()
        this.global.presentToast(JSON.stringify(error),null);
      })
    }
  
  

}
