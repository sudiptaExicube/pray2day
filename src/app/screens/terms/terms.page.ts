import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { GlobalService } from 'src/app/global.service';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.page.html',
  styleUrls: ['./terms.page.scss'],
})
export class TermsPage implements OnInit {

  public showSkleton:boolean = false;
  public privacy:any = null;
  constructor(
    public global:GlobalService,
    public apiservice:ApiService
  ) { }

  ngOnInit() {
    if(this.global.terms){
      this.privacy = this.global.terms;
    }else{
      this.showSkleton = true;
      this.fetch_privacy()
    }
  }

  public fetch_privacy(){
    let params={
      slug:'terms-conditions'
    }
    this.apiservice.api_fetchCMS('cms/contents',params)
    .then((success:any)=>{
      console.log("success :",success)
      this.showSkleton = false
      if(success.data) {
        let data = success.data;
        if(data?.cmscontent){
          this.global.terms = data?.cmscontent
          this.privacy = data?.cmscontent
        }

      }
    })
    .catch((error:any)=>{
      console.log("error :",error)
      this.showSkleton = false
      
    })
  }

  handleRefresh(event:any) {
    let params={
      slug:'terms-conditions'
    }
    this.apiservice.api_fetchCMS('cms/contents',params)
    .then((success:any)=>{
      event.target.complete();
      this.showSkleton = false
      if(success.data) {
        let data = success.data;
        if(data?.cmscontent){
          this.global.terms = data?.cmscontent
          this.privacy = data?.cmscontent
        }

      }
    })
    .catch((error:any)=>{
      event.target.complete();
      this.showSkleton = false
      
    })
  }

}
