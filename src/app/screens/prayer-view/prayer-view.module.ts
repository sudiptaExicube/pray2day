import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PrayerViewPageRoutingModule } from './prayer-view-routing.module';

import { PrayerViewPage } from './prayer-view.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PrayerViewPageRoutingModule
  ],
  declarations: [PrayerViewPage]
})
export class PrayerViewPageModule {}
