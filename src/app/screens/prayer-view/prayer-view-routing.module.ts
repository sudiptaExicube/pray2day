import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PrayerViewPage } from './prayer-view.page';

const routes: Routes = [
  {
    path: '',
    component: PrayerViewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrayerViewPageRoutingModule {}
