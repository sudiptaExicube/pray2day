import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';
import { GlobalService } from 'src/app/global.service';

import { TextToSpeech } from '@capacitor-community/text-to-speech';
import { ApiService } from 'src/app/api.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-prayer-view',
  templateUrl: './prayer-view.page.html',
  styleUrls: ['./prayer-view.page.scss'],
})
export class PrayerViewPage implements OnInit {
  public currentSliderIndex: any = 1;

  @ViewChild(IonSlides) slides:any = IonSlides;

  public showLoading:boolean = true;

  constructor(
    public global:GlobalService,
    private route: Router,
    private activatedRoute:ActivatedRoute,
    private router: Router,
    private apiService: ApiService,
    public sanitizer: DomSanitizer,
    public zone: NgZone
  ) { 
   
  }

  
  public audioMode: any = ''
  public currentSlide = 0;
  public sliderData:any = [
    {des:'How do the words and the picture go together?'},
    {des:`OUR means that something belongs to more than one person, we need to share God with
    OUR brothers and sisters, OUR parents, our friends and everyone in the world. It looks like the children in
    this picture are afraid or uncertain, maybe they do not have a mother or father, they might be orphans. They
    are probably happy that they have God as their father even though they may be afraid or uncertain of other
    things in their lives`},
    {des:`Questions for God, possible answers from God, if I haven’t met somebody, do they still count as OUR?`}
    
  ] 

  // public chaptarDetails:any=[];
  public currentShowableData:any={};
  public showableIndex:number = 0;

  public chapter_id:any;
  public chapterDetails:any={}
  public previousData:any;
  public nextData:any;
  public prayer_line:any={}

  public audioPlayer:any;


  isAudioPlaying: boolean = false;
  audioSource: string = ""

  toggleAudio() {
    this.audioPlayer = document.getElementById('audioPlayer') as HTMLAudioElement;

    if (this.isAudioPlaying) {
      this.audioPlayer.pause();
    } else {
      this.audioPlayer.play();
    }

    this.isAudioPlaying = !this.isAudioPlaying;
  }

  ngOnInit() {
    // this.currentSlide = 0;
    this.activatedRoute.queryParams.subscribe((params:any) => {
      if (params ) {
        if(params.chapter_id){
          this.chapter_id = params.chapter_id;
          let data:any={
            chapter_id:this.chapter_id
          }
          // this.showLoading = true;
          this.fetch_chapterQuotes(data)
        }
      }
    })
  }


  public fetch_chapterQuotes(params:any){
    this.showLoading = true;
    console.log("Fetching chapter : ",params);
    this.apiService.fetch_chapterQuotes(params)
    .then((response:any)=>{
      console.log("prayer view :", response)
      this.showLoading = false;
      if(response.status == "success"){
        if(response.data){
          let data = response.data;
          this.chapterDetails = data?.chapter;
          this.previousData = data?.navigation?.prev;
          this.nextData = data?.navigation?.next
          this.prayer_line = data?.prayer_line;
          this.zone.run(()=>{
            this.audioSource = this.prayer_line.sound_file;
          })
        }
      }else{
        this.global.presentToast(response.message,null);
      }
    })
    .catch((error:any)=>{
      this.showLoading = false;
      this.global.presentToast(JSON.stringify(error),5000);
    })
  }






  slidePrevious(){
    if(this.previousData != null){
      if(this.isAudioPlaying){
        this.audioPlayer.pause();
        this.isAudioPlaying = false;
        this.prayer_line.sound_file = null;
      }
      this.fetch_chapterQuotes(this.previousData);
    }
   
  }

  slideNext(){
    if(this.nextData != null){
      if(this.isAudioPlaying){
        this.audioPlayer.pause();
        this.isAudioPlaying = false;
        this.prayer_line.sound_file = null;
      }
      this.fetch_chapterQuotes(this.nextData);
    }
   
  }

  // And then in ts file
  ionSlideTouchEnd(event:any) {
    console.log(event);
    this.slides.getActiveIndex().then((index: number) => {
        console.log(index);
    });
    // if(this.audioMode == 'play'){
    //   this.audioMode = 'stop';
    //   this.stopAudio ()
    // }
  }
 

  async audioControl (type:any,data:any){
    //const voices = await TextToSpeech.getSupportedVoices();
   // const languages = await TextToSpeech.getSupportedLanguages()
   // console.log(voices)
   // console.log(languages)
   
   
    if(type == 'play'){
      this.audioMode = type;
     await TextToSpeech.speak({
       text: data,
       lang: 'en',
       rate: 0.8,
       pitch: 0.9,
       volume: 1.0,
       voice:1,
       category: 'ambient',
      
     }).then(()=>{
       this.audioMode = 'stop';
       this.stopAudio ()
     });
    }else{
     this.audioMode = type;
     this.stopAudio ()
    }
   
   
  }

  async stopAudio (){
    await TextToSpeech.stop();
  }

}
