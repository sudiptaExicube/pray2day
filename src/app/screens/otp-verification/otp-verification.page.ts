import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ApiService } from 'src/app/api.service';
import { GlobalService } from 'src/app/global.service';

@Component({
  selector: 'app-otp-verification',
  templateUrl: './otp-verification.page.html',
  styleUrls: ['./otp-verification.page.scss'],
})
export class OtpVerificationPage implements OnInit {

  public enableResend: boolean = false;
  public hideTime = 0;
  public mobile_no: any = '';
  public otp: any = {
    input1: '',
    input2: '',
    input3: '',
    input4: '',
  };
  public otpTimer: any = "0";
  public from: any = "";
  public paramData: any = "";

  constructor(
    public global:GlobalService,
    private route: Router,
    private apiService: ApiService,
    private navCtrl:NavController
  ) { }

  ngOnInit() {
  }

  /* === FORWARD TO NEXT FIELD === */
  moveFocus(next: any, event: any, prev: any) {
    console.log(event, prev);
    if (event.key == 'Backspace') {
      prev.setFocus();
    } else {
      next.setFocus();
    }
  }


  /* === Submit OTP === */
  public otpVerification(){
    this.global.presentLoadingDefault()
    let params = {
      // type:'user-register',
      type:this.global.passwordReset_type,
      verification_token:this.global.user_token,
      otp:this.otp.input1 + this.otp.input2 + this.otp.input3 + this.otp.input4
    }
    console.log("OTP verification data : ",params)

    this.apiService.otpVerificationFunction(params)
    .then((success:any)=>{
      this.global.presentLoadingClose();
      console.log("success.message : ",success.message)
      if(success.status == 'success'){
        this.global.presentToast(success.message, null);
        this.global.passwordReset_type = ''
        this.navCtrl.navigateRoot(['login'])
      }else{
        this.global.presentToast(success.message,null);
      }
    })
    .catch((error:any)=>{
      this.global.presentLoadingClose()
      this.global.presentToast(JSON.stringify(error),null);
    })

    //this.route.navigate(['./home-page']);
  }

  /* === RESEND OTP === */
  public resendOtp(){
    this.global.presentLoadingDefault()
    let params={
      verification_token:this.global.user_token
    }
    this.apiService.resendOtpFunction(params)
    .then((success:any)=>{
      this.global.presentLoadingClose()
      if(success.status == 'success'){
        this.global.presentToast(success.message, null)
      }else{
        this.global.presentToast(success.message,null);
      }
    })
    .catch((error:any)=>{
      this.global.presentLoadingClose()
      this.global.presentToast(JSON.stringify(error),null);
    })
  }


}
