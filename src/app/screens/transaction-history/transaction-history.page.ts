import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { GlobalService } from 'src/app/global.service';

@Component({
  selector: 'app-transaction-history',
  templateUrl: './transaction-history.page.html',
  styleUrls: ['./transaction-history.page.scss'],
})
export class TransactionHistoryPage implements OnInit {

 public loading:boolean = false;
 public showSkleton:boolean = false;
 public transaction_history:any=[]

  constructor(
    public global:GlobalService,
    public apiService:ApiService
  ) { }

  ngOnInit() {
    if(this.global.transaction_history.lenth > 0){
      this.transaction_history = this.global.transaction_history;
    }else{
      this.showSkleton = true
      this.fetch_transaction_history();
    }
  }

  public fetch_transaction_history(){
    this.apiService.api_transaction_history()
    .then((success:any)=>{
      this.showSkleton = false;
      console.log("success :", success);
      if(success?.status == "success"){
        let data = success?.data;
        if(data?.reports){
          this.global.transaction_history = data?.reports;
          this.transaction_history = data?.reports;
        }
      }
    })
    .catch((error:any)=>{
      this.showSkleton = false;
      console.log("error :", error);
    })
  }

  /* ===== Invoice details ================= */
  public invoicedetails(item:any){
    this.global.presentLoadingDefault()
    console.log("item : ", item)
    let id = item.id;
    this.apiService.api_transaction_details(id)
    .then((success:any)=>{
      this.global.presentLoadingClose()
      console.log("success :", success);
      if(success?.status == "success"){
        let data = success?.data;
        if(data?.receipt_url){
          window.open(data?.receipt_url);
        }
      }else{
        this.global.presentToast("Transaction recipt not found. Please try again later", null)
      }
    })
    .catch((error:any)=>{
      this.global.presentLoadingClose()
      // this.showSkleton = false;
      console.log("error :", error);
    })

  }



  handleRefresh(event:any) {
    this.apiService.api_transaction_history()
    .then((success:any)=>{
      event.target.complete();
      if(success?.status == "success"){
        let data = success?.data;
        if(data?.reports){
          // this.global.transaction_history = data?.reports;
          // this.transaction_history = data?.reports;
          this.global.transaction_history = [];
          this.transaction_history = [];
        }
      }
    })
    .catch((error:any)=>{
      event.target.complete();
      console.log("error :", error);
    })
  }


}
