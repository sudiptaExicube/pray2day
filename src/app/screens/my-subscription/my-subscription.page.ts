import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ApiService } from 'src/app/api.service';
import { GlobalService } from 'src/app/global.service';

@Component({
  selector: 'app-my-subscription',
  templateUrl: './my-subscription.page.html',
  styleUrls: ['./my-subscription.page.scss'],
})
export class MySubscriptionPage implements OnInit {

  constructor(
    public global:GlobalService,
    public apiservice:ApiService,
    public navCtrl:NavController
  ) { }

  ngOnInit() {
    this.global.presentLoadingDefault()
    this.fetch_my_subscription();
  }

  public fetch_my_subscription(){
    this.apiservice.api_mySubscription()
    .then((response:any) => {
      this.global.presentLoadingClose();
      console.log("response :", response);
      if(response.status == "success") {
        let data = response.data
        if(data?.subscription){
          this.global.subscriptionDetails = data?.subscription;
          // this.global.subscriptionDetails = null
        }else{this.global.subscriptionDetails = null}
      }else{
        this.global.subscriptionDetails = null
      }
    }).
    catch((error:any)=>{ 
      this.global.presentLoadingClose();
      console.log("error :", error); })
  }


  public buyNow(){
    this.navCtrl.navigateRoot(['./subscription']);
  }
}
