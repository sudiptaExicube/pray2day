import { Component } from '@angular/core';

import { MenuController, Platform, NavController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { GlobalService } from './global.service';
import { ApiService } from './api.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Inbox', url: '/folder/Inbox', icon: 'mail' },
    { title: 'Outbox', url: '/folder/Outbox', icon: 'paper-plane' },
    { title: 'Favorites', url: '/folder/Favorites', icon: 'heart' },
    { title: 'Archived', url: '/folder/Archived', icon: 'archive' },
    { title: 'Trash', url: '/folder/Trash', icon: 'trash' },
    { title: 'Spam', url: '/folder/Spam', icon: 'warning' },
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor(
    public global:GlobalService,
    public navCtrl: NavController,
    public apiService:ApiService,
    public platform:Platform,
    public alertController:AlertController
  ) {
    this.initializeApp()
  }

  initializeApp() {
    this.apiService.GlobalDataFunction(false) // False : If you don't show the Loading
    this.platform.ready().then(() => {
      // this.global.setPushNotification();
      this.checkLogin()
      this.fetchCountryList()
    });
  }

  public fetchCountryList() {
    this.apiService.api_fetchCountries()
    .then((response:any) => {
      console.log("response :", response);
      if(response.status == "success") {
        let data = response.data;
        if(data?.countries){
          this.global.countryList = data?.countries;
          // console.log("this.global.countryList : ", this.global.countryList);
        }else{this.global.countryList = []}
      }else{
        this.global.countryList = []
      }
    }).
    catch((error:any)=>{ console.log("error :", error); })
  }

  checkLogin() {
    let userAccessToken = localStorage.getItem('access_token');
    if (userAccessToken) {
      this.global.user_token = userAccessToken;
      let local_userDetails:any = localStorage.getItem("user_details");
      this.global.userdetails = JSON.parse(local_userDetails);
      console.log("user details : ", this.global.userdetails);
      this.getuserDetails()
      this.fetch_my_subscription();
      // this.navCtrl.navigateRoot(['./home-page']);
    } else {
      let getStartedFlag = localStorage.getItem('getStartedFlag');
      if (getStartedFlag) {
        this.navCtrl.navigateRoot(['./login']);
        // this.route.navigate(['./otp-screen']);
      } else {
        this.navCtrl.navigateRoot(['./intro']);
      }

    }
  }

  public getuserDetails() {
    this.apiService.getProfileInfo();
  }


  editProfile(){
    this.global.closeMenu();
    this.navCtrl.navigateRoot(['editprofile'])
  }

  goToPage(page:string){
    if(page == 'logout'){
      this.logoutFunction()
    }else{
      this.global.closeMenu();
      this.navCtrl.navigateRoot([page])
    }

  }

  gotoHomeScreen(){
    // home-page
    this.global.closeMenu();
    if(this.global.subscriptionDetails){
      this.navCtrl.navigateRoot(['./home-page']);
    }else{
      this.show_buySubscriptionAlert();
      // this.navCtrl.navigateRoot(['./subscription']);
    }
  }

  show_buySubscriptionAlert(){
    this.alertController.create({
      header: 'Buy Subscription',
      message: "You don't have any active subscriptions. Please buy a subscription to access exciting benefits",
      buttons: [
        {
          text: 'No',
          handler: () => {
            this.navCtrl.navigateRoot(['editprofile'])
          }
        },
        {
          text: 'Buy',
          handler: () => {
            this.navCtrl.navigateRoot(['./subscription']);
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }

  public logoutFunction(){
    this.alertController.create({
      header: 'Logout',
      message: 'Do you want to logout?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Let me think');
          }
        },
        {
          text: 'Yes!',
          handler: () => {
            localStorage.removeItem("access_token")
            localStorage.removeItem("user_details")
            this.global.user_token = "";
            this.global.userdetails = {};
            this.global.closeMenu();
            this.navCtrl.navigateRoot(['./login']);
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }

  public fetch_my_subscription(){
    this.apiService.api_mySubscription()
    .then((response:any) => {
      console.log("response :", response);
      if(response.status == "success") {
        let data = response.data;

        this.navCtrl.navigateRoot(['./home-page']);
        if(data?.subscription){
          this.global.subscriptionDetails = data?.subscription;

          console.log("this.global.subscriptionDetails : ", this.global.subscriptionDetails)
          // this.global.subscriptionDetails = null
        }else{
          this.global.subscriptionDetails = null;
          console.log("this.global.subscriptionDetails not : ", this.global.subscriptionDetails)
          this.navCtrl.navigateRoot(['./subscription']);
        }
      }else{
        this.global.subscriptionDetails = null;
        this.navCtrl.navigateRoot(['./subscription']);
      }
    }).
    catch((error:any)=>{ 
      console.log("error :", error); })
    }
}
